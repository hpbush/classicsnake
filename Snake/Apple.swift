//
//  Apple.swift
//  Snake
//
//  Created by Hayden Bush on 4/25/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import Foundation
import UIKit

class Apple {
	var appleColor: UIColor = UIColor.red
	var appleCoords: CGRect
	let appleView: UIView
	let gameBoardView: UIView
	var snake: Snake
	
	var acceptableCoordinates: [[Bool]] = [
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
		[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true]
	]
	
	
	init(gameBoardView: UIView, snake: Snake){
		appleCoords = CGRect(x: 180, y: 270, width: 16, height: 16)
		self.gameBoardView = gameBoardView
		appleView = UIView(frame: appleCoords)
		appleView.backgroundColor = appleColor
		gameBoardView.addSubview(appleView)
		self.snake = snake
	}
	
	
	
	func calcNewApplePos(){
		initializeAcceptableCoordinates()
		var currentLink = snake.head
		
		while(currentLink != nil){
			let xIndex = Int(currentLink!.coordinates.origin.x / 18)
			let yIndex = Int(currentLink!.coordinates.origin.y / 18)
			acceptableCoordinates[yIndex][xIndex] = false
			currentLink = currentLink!.nextLink
		}
		
		let candidateCoordinates = generateCoordinateList()
		let newApplePoint: CGPoint = candidateCoordinates[Int(arc4random_uniform(UInt32(candidateCoordinates.count)))]
		appleCoords = CGRect(x: newApplePoint.x, y: newApplePoint.y, width: 16, height: 16)
	}
	
	func initializeAcceptableCoordinates(){
		for y in 0 ... 28{
			for x in 0 ... 20 {
				acceptableCoordinates[y][x] = true
			}
		}
	}
	
	func generateCoordinateList() -> [CGPoint]{
		var coordinateList: [CGPoint] = []
		for y in 0 ... 28{
			for x in 0 ... 20 {
				if acceptableCoordinates[y][x] {
					coordinateList.append(CGPoint(x: x*18, y: y*18))
				}
			}
		}
		return coordinateList
	}
}
