//
//  SettingsViewController.swift
//  Snake
//
//  Created by Connor McKenzie on 4/17/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import UIKit
import CoreData

class SettingsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
	let hexValues = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]
	var colorSelection: [Int] = [0,0,0,0,0,0]
	var game: Game?
	
	@IBOutlet weak var speedStepper: UIStepper!
	@IBOutlet weak var speedLabel: UILabel!
	@IBOutlet weak var colorPicker: UIPickerView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var restoreDefaultButton: UIButton!
	@IBOutlet weak var speedSettingLabel: UILabel!
	@IBOutlet weak var clearLocalDataButton: UIButton!
	@IBOutlet weak var backToMainMenuButton: UIButton!
	
    @IBAction func backToMain(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
	@IBAction func changeSpeed(_ sender: UIStepper) {
		let mainMenu = (presentingViewController as! MainMenuViewController)
		let newSpeed = abs(sender.value)
		mainMenu.settings.setSpeed(speed: newSpeed)
		setLabelText(speed: newSpeed)
	}
	
	@IBAction func restoreDefaultColor(_ sender: UIButton) {
		let defaultColor = UIColor(red: 145/255, green: 192/255, blue: 121/255, alpha: 1)
		(presentingViewController as! MainMenuViewController).settings.setColor(color: defaultColor)
		changeColor()
	}
	
    @IBAction func deleteLocalData(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Are you sure?", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        let deleteAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.destructive, handler:  {
            (alertAction: UIAlertAction!)
            in
            self.deleteData()
        })
        let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil)
        
        alertController.addAction(deleteAction)
        alertController.addAction(noAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		let mainMenu = (presentingViewController as! MainMenuViewController)
		let speed = mainMenu.settings.speed
		setLabelText(speed: speed)
		changeColor()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	func setLabelText(speed: Double){
		if speed == 0.08{
			speedLabel.text = "Hard"
		}else if speed == 0.12 {
			speedLabel.text = "Medium"
		}else{
			speedLabel.text = "Easy"
		}
	}
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return colorSelection.count
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int { // num rows
		return hexValues.count
	}
	
	func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
		let str = hexValues[row]
		let color = (presentingViewController as! MainMenuViewController).settings.gameColor
		return NSAttributedString(string: "\(str)", attributes: [NSForegroundColorAttributeName: color])
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) { // selected row, take action
		let mainMenu = (presentingViewController as! MainMenuViewController)
		colorSelection[component] = row
		mainMenu.settings.setColor(color: getColorFromSelection())
		changeColor()
	}
	
	func getColorFromSelection() -> UIColor{
		let redStr: String = hexValues[colorSelection[0]] + hexValues[colorSelection[1]]
		let greenStr: String = hexValues[colorSelection[2]] + hexValues[colorSelection[3]]
		let blueStr: String = hexValues[colorSelection[4]] + hexValues[colorSelection[5]]
		let red = (Int)(strtoul(redStr, nil, 16))
		let green = (Int)(strtoul(greenStr, nil, 16))
		let blue = (Int)(strtoul(blueStr, nil, 16))
		let color = UIColor(red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: 1)
		return color
		
	}
	
	func changeColor(){
		let newColor = (presentingViewController as! MainMenuViewController).settings.gameColor
		speedLabel.textColor = newColor
		speedStepper.tintColor = newColor
		speedLabel.textColor = newColor
		titleLabel.textColor = newColor
		restoreDefaultButton.setTitleColor(newColor, for: .normal)
		speedSettingLabel.textColor = newColor
		clearLocalDataButton.setTitleColor(newColor, for: .normal)
		backToMainMenuButton.setTitleColor(newColor, for: .normal)
		colorPicker.reloadAllComponents()
	}
	
	func deleteData(){
		let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
		let entityDescription = NSEntityDescription.entity(forEntityName: "LocalHighScores", in: managedObjectContext)
		
		let request: NSFetchRequest<LocalHighScores> = LocalHighScores.fetchRequest()
		request.entity = entityDescription
		
		do{
			let results = try managedObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>)
			
			
			for item in results {
				managedObjectContext.delete(item as! NSManagedObject)
				try managedObjectContext.save()
			}
			
		}catch let error{
			print(error.localizedDescription)
		}
	}
	
}
