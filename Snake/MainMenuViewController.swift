//
//  MainMenuViewController.swift
//  Snake
//
//  Created by Connor McKenzie on 4/17/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import UIKit
import CoreData

class MainMenuViewController: UIViewController {
    
	var game: Game?
	var settings: Settings = Settings()
	var userPressedContinue: Bool = false
	
	@IBOutlet weak var continueButton: UIButton!
	
    @IBAction func goToGame(_ sender: UIButton) {
		if game != nil {
			userPressedContinue = false
		}
		
        performSegue(withIdentifier: "toGame", sender: sender)
    }
    
    @IBAction func goToHighScores(_ sender: UIButton) {
        performSegue(withIdentifier: "toHighScores", sender: sender)
    }
    
    @IBAction func goToSettings(_ sender: UIButton) {
        performSegue(withIdentifier: "toSettings", sender: sender)
    }
    
    @IBAction func displayHowToPlay(_ sender: UIButton) {
        
    }
    
    @IBAction func continueGame(_ sender: UIButton) {
		userPressedContinue = true
        performSegue(withIdentifier: "toGame", sender: sender)
		
    }
    
    @IBAction func howToPlay(_ sender: UIButton) {
        let alertController = UIAlertController(title: "How to play", message: "Swipe to move the snake in a certain direction, and collect the dots to grow in size. \n\n You lose when you hit a wall or your own body.", preferredStyle: UIAlertControllerStyle.alert)
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
        
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.view.backgroundColor = settings.gameColor
		checkForSavedGame()
	}
	
	func checkForSavedGame(){
		let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
		let entityDescription = NSEntityDescription.entity(forEntityName: "GameData", in: managedObjectContext)
		
		let request: NSFetchRequest<GameData> = GameData.fetchRequest()
		request.entity = entityDescription
		
		do{
			let results = try managedObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>)
			if results.count > 0 {
				continueButton.isHidden = false
				continueButton.isEnabled = true
			}else{
				continueButton.isHidden = true
				continueButton.isEnabled = false
			}
		}catch let error{
			print(error.localizedDescription)
		}
	}
	
	
}
