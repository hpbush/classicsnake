//
//  Settings.swift
//  Snake
//
//  Created by Hayden Bush on 4/30/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import Foundation
import UIKit

class Settings{
	var speed: Double = 0.12
	var gameColor: UIColor = UIColor(red: 145/255, green: 192/255, blue: 121/255, alpha: 1)
	
	func setColor(color: UIColor){
		self.gameColor = color
	}
	
	func setSpeed(speed: Double){
		self.speed = speed
	}
}
