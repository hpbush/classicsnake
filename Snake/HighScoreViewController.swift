//
//  HighScoreViewController.swift
//  Snake
//
//  Created by Connor McKenzie on 4/17/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import UIKit
import CoreData

class HighScoreViewController: UIViewController {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var backToMenuButton: UIButton!
    
    @IBAction func backToMain(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		changeColor()
		
		let highScoreList = getHighScoreList()

		if highScoreList!.count > 0 {
			let sortedScores = bubbleSort(scores: highScoreList!)
			createScoreLabels(scores: sortedScores)
		}
		
		
		
	}
	
	func changeColor(){
		let color = (presentingViewController as! MainMenuViewController).settings.gameColor
		titleLabel.textColor = color
		backToMenuButton.setTitleColor(color, for: .normal)
	}
	
	func getHighScoreList() -> [LocalHighScores]? {
		let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
		let entityDescription = NSEntityDescription.entity(forEntityName: "LocalHighScores", in: managedObjectContext)
		
		let request: NSFetchRequest<LocalHighScores> = LocalHighScores.fetchRequest()
		request.entity = entityDescription
		
		do{
			let results = try managedObjectContext.fetch(request)
			return results
		}catch let error{
			print(error.localizedDescription)
		}
		return nil
	}
	
	
	func bubbleSort(scores: [LocalHighScores]) -> [LocalHighScores]{
		var sortedScores = scores
		
		for i in 0 ..< sortedScores.count-1 {
			for j in 0 ..< sortedScores.count-i-1{
				if sortedScores[j].score < sortedScores[j+1].score {
					let temp = sortedScores[j]
					sortedScores[j] = sortedScores[j+1]
					sortedScores[j+1] = temp
				}
			}
		}
		return sortedScores
	}
	
	func createScoreLabels(scores: [LocalHighScores]){
		let mainMenu = (presentingViewController as! MainMenuViewController)
		for i in 0 ..< scores.count {
			let attributes = [NSFontAttributeName : UIFont(name: "ArcadeClassic", size: 18)!]
			
			let initialsLabelCoords = CGRect(x: 100, y: 125 + i*50, width: 300, height: 21)
			let scoreLabelCoords = CGRect(x: 250, y: 125 + i*50, width: 300, height: 21)
			
			let initialsLabel =  UILabel(frame: initialsLabelCoords)
			let scoreLabel = UILabel(frame: scoreLabelCoords)
			
			let initialsLabelText = "\(i+1).   \(scores[i].name!)"
			let scoreLabelText = "\(scores[i].score)"
			
			initialsLabel.attributedText = NSAttributedString(string: initialsLabelText, attributes: attributes)
			scoreLabel.attributedText = NSAttributedString(string: scoreLabelText, attributes: attributes)
			
			initialsLabel.textColor = mainMenu.settings.gameColor
			scoreLabel.textColor = mainMenu.settings.gameColor
			
			self.view.addSubview(initialsLabel)
			self.view.addSubview(scoreLabel)
			
		}
	}
}
