//
//  GameArtist.swift
//  Snake
//
//  Created by Hayden Bush on 4/27/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import Foundation
import UIKit

class GameArtist{
	
	var snake: Snake
	let apple: Apple
	var settings: Settings?
	
	init(snake: Snake, apple: Apple){
		self.snake = snake
		self.apple = apple
	}
	
	func drawSnake(){
		var currentLink: SnakeBody? = snake.head!
		
		while(currentLink != nil){
			currentLink!.view.frame = currentLink!.coordinates
			currentLink!.view.backgroundColor = settings!.gameColor
			currentLink = currentLink!.nextLink
		}
	}
	
	func drawApple(){
		apple.appleView.frame = apple.appleCoords
	}
}
