//
//  Snake.swift
//  Snake
//
//  Created by Hayden Bush on 4/24/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import Foundation
import UIKit

class Snake{
	var isAlive: Bool
	var head: SnakeBody?
	var tail: SnakeBody?
	let gameBoardView: UIView
	var moveDirection = 1  // 1: right, 4: up, 2: left, 8: down
	
	init(gameBoardView: UIView){
		self.gameBoardView = gameBoardView
		isAlive = true
	}
	
	func createStartSnake(){
		
		
		
		var coordinates = CGRect(x: 180, y: 360, width: 16, height: 16)
		var temp = SnakeBody(coordinates: coordinates, gameBoardView: gameBoardView)
		addSnakeBody(newTail: temp)
		
		coordinates = CGRect(x: 162, y: 360, width: 16, height: 16)
		temp = SnakeBody(coordinates: coordinates, gameBoardView: gameBoardView)
		addSnakeBody(newTail: temp)
		
		coordinates = CGRect(x: 144, y: 360, width: 16, height: 16)
		temp = SnakeBody(coordinates: coordinates, gameBoardView: gameBoardView)
		addSnakeBody(newTail: temp)
		
	}
	
	func updatePositions() -> CGRect{
		var nextLinksFutureLoc = head!.coordinates
		if updateHeadPosition(head: self.head!){
			return CGRect(x: 0, y: 0, width: 0, height: 0)  // if head is out of bounds, prevent execution of the updatepositions function.
		}
		var currentLink: SnakeBody? = head!.nextLink
		var currentLinksOldLoc = currentLink!.coordinates
		while(currentLink != nil){
			currentLinksOldLoc = currentLink!.coordinates
			currentLink!.coordinates = nextLinksFutureLoc
			
			if(headHitBody(bodyCoord: nextLinksFutureLoc)){ //if head collides with the coordinates of the currentLink, call game over, and finish tick
				isAlive = false
			}
			
			nextLinksFutureLoc = currentLinksOldLoc
			currentLink = currentLink!.nextLink
		}
		return currentLinksOldLoc
	}
	
	func updateHeadPosition(head: SnakeBody) -> Bool{
		let originalCoordinates = head.coordinates
		
		if moveDirection == 1 {
			head.coordinates = CGRect(x: originalCoordinates.origin.x + 18, y: originalCoordinates.origin.y, width: 16, height: 16)
		}else if moveDirection == 4 {
			head.coordinates = CGRect(x: originalCoordinates.origin.x, y: originalCoordinates.origin.y - 18, width: 16, height: 16)
		}else if moveDirection == 2{
			head.coordinates = CGRect(x: originalCoordinates.origin.x - 18, y: originalCoordinates.origin.y, width: 16, height: 16)
		}else if moveDirection == 8{
			head.coordinates = CGRect(x: originalCoordinates.origin.x, y: originalCoordinates.origin.y + 18, width: 16, height: 16)
		}
		
		if headOutOfBounds(newCoords: head.coordinates){
			isAlive = false
			head.coordinates = originalCoordinates
			return true
		}
		
		return false
	}
	
	func checkValidMoveDirection(newDirection: Int) -> Bool{
		if moveDirection < 3 {
			return newDirection > 3
		}
		return newDirection < 3
	}
	
	func addToTail(newTailCoord: CGRect){
		let newTail = SnakeBody(coordinates: newTailCoord, gameBoardView: gameBoardView)
		tail!.nextLink = newTail
		tail = newTail
	}
	
	func headOutOfBounds(newCoords: CGRect) -> Bool {
		if newCoords.origin.x < 0 || newCoords.origin.x > 378 - 18 {
			return true
		}
		return newCoords.origin.y < 0 || newCoords.origin.y > 522 - 18
	}
	
	func headHitBody(bodyCoord: CGRect) -> Bool {
		return head!.coordinates == bodyCoord
	}
	
	func addSnakeBody(newTail: SnakeBody){
		if head == nil {
			head = newTail
			tail = newTail
		}else{
			tail!.nextLink = newTail
			tail = newTail
		}
	}
}


