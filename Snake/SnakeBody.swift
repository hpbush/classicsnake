//
//  SnakeBody.swift
//  Snake
//
//  Created by Hayden Bush on 4/24/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import Foundation
import UIKit

class SnakeBody{
	
	var nextLink: SnakeBody?
	var coordinates: CGRect
	var view: UIView
	let gameBoardView: UIView
	
	init(coordinates: CGRect, gameBoardView: UIView){
		self.gameBoardView = gameBoardView
		self.coordinates = coordinates
		self.view = UIView(frame: coordinates)
		gameBoardView.addSubview(self.view)
		nextLink = nil
	}
}
