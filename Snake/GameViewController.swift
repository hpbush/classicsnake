//
//  GameViewController.swift
//  Snake
//
//  Created by Connor McKenzie on 4/17/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
	
	@IBOutlet weak var gameView: UIView!
	@IBOutlet weak var scoreLabel: UILabel!
	
	var game: Game?
	

	@IBAction func swipeGesture(_ sender: UISwipeGestureRecognizer) {
		let direction = Int(sender.direction.rawValue)
		if game!.snake.checkValidMoveDirection(newDirection: direction){
			game!.snake.moveDirection = direction
		}
	}
	
    @IBAction func paused(_ sender: UIButton) {
		game!.paused = true
        let alertController = UIAlertController(title: "Game Paused", message: "Do you wish to continue?", preferredStyle: UIAlertControllerStyle.alert)
		let continueAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.cancel, handler: {
			(alertAction: UIAlertAction!)
			in
			self.game!.paused = false
		})
        let saveAndQuitAction = UIAlertAction(title: "Save and Quit", style: UIAlertActionStyle.default, handler: {
            (alertAction: UIAlertAction!)
            in
			self.game!.saveGame()
            self.dismiss(animated: true, completion: nil)})
        
        alertController.addAction(continueAction)
        alertController.addAction(saveAndQuitAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
		
		
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		let mainMenu = (presentingViewController as! MainMenuViewController)
		if(game == nil){
			initializeGame()
		}

		if mainMenu.userPressedContinue == true{
			game!.loadGame()
			game!.timer.invalidate()
			game!.gameLoop()

		}
		self.view.backgroundColor = game!.settings!.gameColor
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
	
	func initializeGame(){
		let newGame = Game(gameBoardView: gameView, scoreLabel: scoreLabel)
		newGame.gameViewController = self
		//print(presentingViewController)
		let mainMenu = (presentingViewController as! MainMenuViewController)
		newGame.setSettings(settings: mainMenu.settings)
		game = newGame
		mainMenu.game = newGame
		game!.gameLoop()
	}
}
