//
//  Game.swift
//  Snake
//
//  Created by Hayden Bush on 4/24/17.
//  Copyright © 2017 Hayden Connor Tim CSC 308. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Game{
	var snake: Snake
	var apple: Apple
	var gameArtist: GameArtist
	var settings: Settings?
    let gameBoardView: UIView
	var timer = Timer()
	let scoreLabel: UILabel
	var paused: Bool
	var score: Int
	var gameViewController: GameViewController?
	
	
	init(gameBoardView: UIView, scoreLabel: UILabel){
		snake = Snake(gameBoardView: gameBoardView)
		snake.createStartSnake()
        self.gameBoardView = gameBoardView
		apple = Apple(gameBoardView: gameBoardView, snake: snake)
		gameArtist = GameArtist(snake: snake, apple: apple)
		score = 0
		self.scoreLabel = scoreLabel
		paused = false
	}
	
	func gameLoop(){
		timer = Timer.scheduledTimer(withTimeInterval: settings!.speed, repeats: true, block: {
			(Timer)
			in
			self.tick()
		})
	}
	
	func tick(){
		if !paused{
			update()
			render()
			if !snake.isAlive {
				gameOver()
			}
		}
	}
	
	func update(){
		let oldTailCoord = snake.updatePositions()
		if appleHit() {
			apple.calcNewApplePos()
			snake.addToTail(newTailCoord: oldTailCoord)
			addScore()
		}
	}
	
	func render(){
		gameArtist.drawApple()
		gameArtist.drawSnake()
	}
	
	func appleHit() -> Bool{
		return snake.head!.coordinates == apple.appleCoords
	}
	
	func addScore(){
		score = score + 100 * Int( (5 - 25 * (settings!.speed)) )
		scoreLabel.text = "\(score)"
	}
	
	func gameOver(){
		timer.invalidate()
		
		if checkIfHighScore(newScore: score){
			let alertController = UIAlertController(title: "High Score!", message: "Enter your initials.", preferredStyle: UIAlertControllerStyle.alert)
			
			alertController.addTextField(configurationHandler: {
				(textField: UITextField)
				in
				textField.placeholder = ""
				textField.keyboardType = UIKeyboardType.alphabet
			})
			
			let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
				(UIAlertAction)
				in
				self.gameViewController!.dismiss(animated: true, completion: {
					()
					in
					let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
					let entityDescription = NSEntityDescription.entity(forEntityName: "LocalHighScores", in: managedObjectContext)
					
					let request: NSFetchRequest<LocalHighScores> = LocalHighScores.fetchRequest()
					request.entity = entityDescription
					
					do{
						let results = try managedObjectContext.fetch(request)
						if results.count == 10{
							var lowest = results[0]
							for i in 1 ..< results.count{
								if results[i].score < lowest.score{
									lowest = results[i]
								}
							}
							managedObjectContext.delete(lowest as NSManagedObject)
							try managedObjectContext.save()
						}
						let newHighScore = LocalHighScores(entity: entityDescription!, insertInto: managedObjectContext)
						newHighScore.name = alertController.textFields![0].text!
						newHighScore.score = Int32(self.score)
						try managedObjectContext.save()
					}catch let error{
						print(error.localizedDescription)
					}
					
				})
			})
			
			alertController.addAction(defaultAction)
			gameViewController!.present(alertController, animated: true, completion: nil)
		}else{
			let alertController = UIAlertController(title: "Game Over", message: "Better luck next time", preferredStyle: UIAlertControllerStyle.alert)
			
			let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
				(UIAlertAction)
				in
				self.gameViewController!.dismiss(animated: true, completion: nil)
			})
			
			alertController.addAction(defaultAction)
			gameViewController!.present(alertController, animated: true, completion: nil)
		}
		
		
	}
	
	func setSettings(settings: Settings){
		self.settings = settings
		gameArtist.settings = settings
	}
	
	func saveGame(){
		let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
		let entityDescription = NSEntityDescription.entity(forEntityName: "GameData", in: managedObjectContext)
		let savedGame = GameData(entity: entityDescription!, insertInto: managedObjectContext)
		
		savedGame.appleX = Int16(apple.appleCoords.origin.x)
		savedGame.appleY = Int16(apple.appleCoords.origin.y)
		savedGame.moveDirection = Int16(snake.moveDirection)
		savedGame.score = Int32(score)
		savedGame.speed = settings!.speed
		
		savedGame.snakeBody = convertSnakeToCoords() as NSArray
		
		do {
			try managedObjectContext.save()
		}catch let error{
			print(error.localizedDescription)
		}
	}
	
	func loadGame(){
		let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
		let entityDescription = NSEntityDescription.entity(forEntityName: "GameData", in: managedObjectContext)
		
		let request: NSFetchRequest<GameData> = GameData.fetchRequest()
		request.entity = entityDescription
		
		do{
			var results = try managedObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>)
			if results.count > 0 {
				let match = results[0] as! GameData
				apple.appleCoords = CGRect(x: Int(match.appleX), y: Int(match.appleY), width: 16, height: 16)
				score = Int(match.score)
				scoreLabel.text = "\(score)"
				settings!.speed = match.speed
				
				snake = buildSnakeFromLoad(match: match)
				snake.moveDirection = Int(match.moveDirection)
				
				gameArtist.snake = snake
				apple.snake = snake
			}
			
			for item in results {
				managedObjectContext.delete(item as! NSManagedObject)
				try managedObjectContext.save()
			}
			
		}catch let error{
			print(error.localizedDescription)
		}
	}
	
	func buildSnakeFromLoad(match: GameData) -> Snake{
		let newSnake = Snake(gameBoardView: gameBoardView)
		let snakeCoords = match.snakeBody as! NSArray
		var x: CGFloat = 0
		var y: CGFloat = 0
		
		for i in 0 ..< snakeCoords.count {
			
			
			if i % 2 == 0 {
				x = snakeCoords[i] as! CGFloat
			}else{
				y = snakeCoords[i] as! CGFloat
				let tempCoords = CGRect(x: x, y: y, width: 0, height: 0)
				newSnake.addSnakeBody(newTail: SnakeBody(coordinates: tempCoords, gameBoardView: gameBoardView))
			}
		}
		return newSnake
	}
	
	func convertSnakeToCoords() -> [Int] {
		var coords: [Int] = []
		
		var currentLink = snake.head
		while(currentLink != nil){
			let xValue = currentLink!.coordinates.origin.x
			let yValue = currentLink!.coordinates.origin.y
			coords.append(Int(xValue))
			coords.append(Int(yValue))
			currentLink = currentLink!.nextLink
		}
		
		return coords
	}
	
	func checkIfHighScore(newScore: Int) -> Bool{
		let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
		let entityDescription = NSEntityDescription.entity(forEntityName: "LocalHighScores", in: managedObjectContext)
		
		let request: NSFetchRequest<LocalHighScores> = LocalHighScores.fetchRequest()
		request.entity = entityDescription
		
		do{
			let results = try managedObjectContext.fetch(request)
			if results.count < 10{
				return true
			}else{
				for i in 0 ..< results.count{
					if Int32(newScore) > results[i].score{
						return true
					}
				}
				return false
			}
			

			
		}catch let error{
			print(error.localizedDescription)
		}
		return false
	}
}
